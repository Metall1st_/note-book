﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using note = Note.Note;
using interFace = Interface.Interface;

namespace NoteBook
{
    class NoteBook
    {
        string a = "lastname firstname middlename number";
        static void Main(string[] args)
        {
            List<note> notes = new List<note>();
            List<note> foundNotes = new List<note>();
            
            string stateOfShow = "full";
            while (true)
            {
                string id = null;
                int index;

                note.ShowNotes(stateOfShow, notes, foundNotes);
                interFace.MainMenu(notes.Count);
                string command = interFace.CommandDefine(notes.Count);
                switch (command)
                {
                    case "add":
                        Dictionary<string, string> newNote = interFace.Adding();
                        notes.Add
                            (
                                new note(
                                    Convert.ToInt64(newNote["number"]),
                                    newNote["country"],
                                    newNote["lastname"],
                                    newNote["firstname"],
                                    newNote["middlename"],
                                    Convert.ToDateTime(newNote["birth"]),
                                    newNote["organisation"],
                                    newNote["position"],
                                    newNote["other"]
                                )
                            );
                        break;
                    case "showfull":
                        stateOfShow = "full";
                        break;
                    case "showshort":
                        stateOfShow = "short";
                        break;
                    case "find":
                        List<string> fieldsToSearch = interFace.DefineNoteFields("find");
                        Dictionary<string, string> description = interFace.Finding(fieldsToSearch);

                        foundNotes = note.FindNote(notes, description);
                        stateOfShow = "found";
                        break;
                    case "edit":
                        interFace.Editing(ref id, notes.Count);

                        index = Convert.ToInt32(id) - 1;
                        List<string> fieldsToEdit = interFace.DefineNoteFields("edit");

                        foreach (var fieldToEdit in fieldsToEdit)
                            notes[index] = note.EditNote(notes[index], fieldToEdit, interFace.ChangeNoteField(fieldToEdit));
                        break;
                    case "delete":
                        interFace.Editing(ref id, notes.Count);
                        index = Convert.ToInt32(id) - 1;

                        note.DeleteNote(notes, notes[index].id);
                        break;
                    case "exit":
                        return;
                    default:
                        break;
                }
            }
        }
    }
}
