﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public static class Interface
    {
        public static Dictionary<string, string> possibleCommands = new Dictionary<string, string>()
            {
                { "add", "To add a new contact to your Note Book." },
                { "showfull", "To show full contact list." },
                { "showshort", "To show short contact list (Last name, First name, Phone number)." },
                { "find", "To find a contact in your Note book." },
                { "edit", "To edit a contact in your Note Book." },
                { "delete", "To remove a contact from your Note Book." },
                { "exit", "To quit the programm." }
            };

        public static List<string> fields = new List<string>()
            {
                "lastname",
                "firstname",
                "middlename",
                "number",
                "country",
                "birth",
                "organisation",
                "position",
                "other"
            };

        public static void MainMenu(int notesCount)
        {
            Console.WriteLine("\nPossible commands: ");
            if (notesCount > 0)
                foreach (var cmd in possibleCommands.Keys)
                    Console.WriteLine("{0}: {1}", cmd, possibleCommands[cmd]);
            else
                foreach (var cmd in possibleCommands.Keys)
                {
                    if (cmd == "add")
                        Console.WriteLine("{0}: {1}", "add", possibleCommands["add"]);
                    else if (cmd == "exit")
                        Console.WriteLine("{0}: {1}", "exit", possibleCommands["exit"]);
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        Console.WriteLine("{0}: {1}", cmd, possibleCommands[cmd]);
                        Console.ResetColor();
                    }
                }

        }

        // Deciding what command to do
        public static string CommandDefine(int notesCount)
        {
            string cmd = Console.ReadLine().ToLower().Trim();
            while ((!possibleCommands.ContainsKey(cmd) && notesCount != 0 || cmd != "add" && notesCount == 0) && cmd != "exit")
            {
                Console.Write("Invalid command! Try again: ");
                cmd = Console.ReadLine().ToLower().Trim();
            }

            return cmd.ToLower();
        }

        // Decidig which fields to define
        public static Dictionary<string, string> Adding()
        {
            Console.Clear();
            Dictionary<string, string> note = new Dictionary<string, string>();
            foreach (var field in fields)
            {
                bool isOptional = field == fields[2] || field == fields[5] || field == fields[6] || field == fields[7] || field == fields[8];
                string optional = "(Optional: press Enter to skip)";
                Console.Write("{0}{1}: ", field, isOptional ? optional : "");

                if (field == "number")
                {
                    while (true)
                    {
                        string value = Console.ReadLine();
                        try
                        {
                            if (Convert.ToInt64(value) > 0)
                            {
                                note.Add(field, value.Trim());
                                break;
                            }
                            else
                            {
                                Console.Write("Invalid number! Try again: ");
                                continue;
                            }

                        }
                        catch (Exception)
                        {
                            Console.Write("Invalid number! Try again: ");
                            continue;
                        }
                    }
                }
                else if (field == "birth")
                {
                    while (true)
                    {
                        string value = Console.ReadLine();
                        try
                        {
                            if (value.Trim() == String.Empty)
                            {
                                note.Add(field, null);
                                break;
                            }
                            else
                            {
                                if (Convert.ToDateTime(value).Second >= 0)
                                {
                                    note.Add(field, value.Trim());
                                    break;
                                }
                                else
                                {
                                    Console.Write("Invalid date of Birtg! Try again: ");
                                    continue;
                                }
                            }

                        }
                        catch (Exception)
                        {
                            Console.Write("Invalid date of Birtg! Try again: ");
                            continue;
                        }
                    }
                }
                else
                {
                    if (isOptional)
                    {
                        string value = Console.ReadLine();
                        if (value.Trim() == String.Empty)
                            note.Add(field, null);
                        else
                            note.Add(field, value.Trim());
                    }
                    else
                    {
                        while (true)
                        {
                            string value = Console.ReadLine();
                            if (value.Trim() != String.Empty)
                            {
                                note.Add(field, value.Trim());
                                break;
                            }
                            Console.Write("Invalid value for required field '{0}'! Try again: ", field);
                        }
                    }
                }
            }

            return note;
        }

        // Deciding what parameters to search for
        public static Dictionary<string, string> Finding(List<string> fieldsToSearch)
        {
            Dictionary<string, string> description = new Dictionary<string, string>();
            foreach (var fieldToSearch in fieldsToSearch)
            {
                Console.Write("Enter the '{0}' of contact you want to find: ", fieldToSearch);
                string answer;
                while(true)
                {
                    answer = Console.ReadLine();
                    if (fieldToSearch == "number")
                    {
                        try
                        {
                            long number = Convert.ToInt64(answer);
                            break;
                        }
                        catch (Exception)
                        {
                            Console.Write("Invalid number! Try again: ");
                        }
                    }
                    else if (fieldToSearch == "birth")
                    {
                        try
                        {
                            DateTime birth = Convert.ToDateTime(answer);
                            break;
                        }
                        catch (Exception)
                        {
                            Console.Write("Invalid date of Birth! Try again: ");
                        }
                    }
                    else
                        break;
                }

                description.Add(fieldToSearch, answer);
            }

            return description;
        }

        // Deciding what Note to edit
        public static void Editing(ref string answer, int notesCount)
        {
            Console.WriteLine("\nEnter the ID of the note you want to edit/delete: ");
            answer = Console.ReadLine();
            while (Convert.ToInt32(answer) > notesCount)
            {
                Console.WriteLine("Wrong ID!\nEnter the ID of the note you want to edit/delete again: ");
                answer = Console.ReadLine();
            }
            Console.WriteLine();
        }

        // Deciding what fields to edit/search/etc. in the Note
        public static List<string> DefineNoteFields(string action = "edit")
        {
            Console.WriteLine("\nPossible commands:");

            foreach (var field in fields)
                Console.WriteLine(field);
            Console.WriteLine();

            while (true)
            {
                List<string> commands = new List<string>();

                Console.Write("Select one or more options to {0}. Use space to separate items: ", action);

                commands = Console.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                bool hasWrongCommands = false;

                commands.ConvertAll(command => command.ToLower());

                foreach (var command in commands)
                {
                    if (!fields.Contains(command.ToString()))
                    {
                        hasWrongCommands = true;
                        break;
                    }
                }


                if (!hasWrongCommands)
                    return commands;
            }
        }

        // Deciding how to edit specific field of Note
        public static string ChangeNoteField(string command)
        {
            Console.Write("Enter new {0} for your contact: ", command);

            string changedField;
            if (command == "number")
                while (true)
                {
                    changedField = Console.ReadLine();
                    try
                    {
                        return Convert.ToString(Convert.ToInt64(changedField));
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("This phone number is not valid!");
                        Console.Write("Enter new {0} for your contact: ", command);
                    }
                }
            if (command == "birth")
                while (true)
                {
                    changedField = Console.ReadLine();
                    try
                    {
                        return Convert.ToString(Convert.ToDateTime(changedField));
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("This date of Birth is not valid!");
                        Console.Write("Enter new date of Birth for your contact: ");
                    }
                }
            changedField = Console.ReadLine();
            return changedField;
        }
    }
}
