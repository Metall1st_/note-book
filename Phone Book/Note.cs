﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Note
{
    class Note
    {
        public int id { get; private set; }
        private static int notesCount = 1;

        private DateTime creationTime;

        private string lastName, firstName, middleName;
        private long phoneNumber;
        private string country;
        private DateTime dateOfBirth;
        private string organisation, position, other;

        public Note(long pn, string c, string ln, string fn, string mn = null, DateTime dob = new DateTime(), string org = null, string pos = null, string o = null)
        {
            if (pn == 0 || c == null || c == "" || ln == null || ln == "" || fn == null || fn == "")
                return;

            id = notesCount;
            lastName = ln;
            firstName = fn;
            middleName = mn;
            phoneNumber = pn;
            country = c;
            dateOfBirth = dob;
            organisation = org;
            position = pos;
            other = o;

            creationTime = DateTime.Now;

            notesCount++;
        }

        public static Note EditNote(Note note, string command, string changedField)
        {
            switch (command)
            {
                case "lastname":
                    note.lastName = changedField;
                    break;
                case "firstname":
                    note.firstName = changedField;
                    break;
                case "middlename":
                    note.middleName = changedField;
                    break;
                case "number":
                    note.phoneNumber = Convert.ToInt64(changedField);
                    break;
                case "country":
                    note.country = changedField;
                    break;
                case "birth":
                    note.dateOfBirth = Convert.ToDateTime(changedField);
                    break;
                case "organisation":
                    note.organisation = changedField;
                    break;
                case "position":
                    note.position = changedField;
                    break;
                case "other":
                    note.other = changedField;
                    break;
                default:
                    break;
            }
            return note;
        }

        public static List<Note> FindNote(List<Note> notes, Dictionary<string, string> descriptions, bool isStrong = false)
        {
            List<Note> foundNotes = new List<Note>();
            descriptions.Values.ToList().ConvertAll(desc => desc.ToLower());

            if (isStrong)   // isStrong means that all fields must contain the certain value. 
            {               // If isStrong == false => one of the fields could contain the value and it still could be found
            
            }
            else
            {
                foreach (var desc in descriptions)
                {
                    foreach (var note in notes)
                    {
                        switch (desc.Key)
                        {
                            case "lastname":
                                if (desc.Value == note.lastName.ToLower() && !foundNotes.Contains(note))
                                    foundNotes.Add(note);
                                break;
                            case "firstname":
                                if (desc.Value == note.firstName.ToLower() && !foundNotes.Contains(note))
                                    foundNotes.Add(note);
                                break;
                            case "middlename":
                                if (note.middleName != null)
                                    if (desc.Value == note.middleName.ToLower() && !foundNotes.Contains(note))
                                        foundNotes.Add(note);
                                break;
                            case "number":
                                if (Convert.ToInt64(desc.Value) == note.phoneNumber && !foundNotes.Contains(note))
                                    foundNotes.Add(note);
                                break;
                            case "country":
                                if (desc.Value == note.country.ToLower() && !foundNotes.Contains(note))
                                    foundNotes.Add(note);
                                break;
                            case "birth":
                                if (Convert.ToDateTime(desc.Value) == note.dateOfBirth && !foundNotes.Contains(note))
                                    foundNotes.Add(note);
                                break;
                            case "organisation":
                                if (note.organisation != null)
                                    if (desc.Value == note.organisation.ToLower() && !foundNotes.Contains(note))
                                        foundNotes.Add(note);
                                break;
                            case "position":
                                if (note.position != null)
                                    if (desc.Value == note.position.ToLower() && !foundNotes.Contains(note))
                                        foundNotes.Add(note);
                                break;
                            case "other":
                                if (note.other != null)
                                    if (desc.Value == note.other.ToLower() && !foundNotes.Contains(note))
                                        foundNotes.Add(note);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            return foundNotes;
        }

        public static void DeleteNote(List<Note> notes, int id)
        {
            notes.Remove(notes[id - 1]);
            ChangeId(ref notes, id);
        }

        public static void ShowNotes(string state, List<Note> notes, List<Note> foundNotes)
        {
            Console.Clear();
            if (notes.Count <= 0 && state != "found")
            {
                Console.WriteLine("There is no contacts yet...");
                return;
            }
            if (state == "found" && foundNotes.Count <= 0)
            {
                Console.WriteLine("There is no such Notes in your Note Book.");
                return;
            }

            if (state == "full")
            {
                for (int i = 0; i < notes.Count; i++)
                {
                    Note n = notes[i];
                    Console.Write("{0}. {1} {2}{5}. Phone number: +{3}. {4}. ", n.id, n.lastName, n.firstName, n.phoneNumber, n.country, n.middleName != null ? " " + n.middleName : "");
                    if (n.dateOfBirth != new DateTime())
                        Console.Write("Date of Birth: {0}. ", n.dateOfBirth);
                    if (n.organisation != null && n.organisation != "")
                        Console.Write("Organisation: {0}. ", n.organisation);
                    if (n.position != null && n.position != "")
                        Console.Write("Position: {0}. ", n.position);
                    if (n.other != null && n.other != "")
                        Console.Write("\nOther: {0}", n.other);
                    Console.WriteLine();
                }
            }
            else if (state == "short")
            {
                for (int i = 0; i < notes.Count; i++)
                {
                    Note n = notes[i];
                    Console.WriteLine("{0}. {1} {2} Phone number: +{3} ", n.id, n.lastName, n.firstName, n.phoneNumber);
                }
            }
            else if (state == "found" && foundNotes.Count > 0)
            {
                try
                {
                    for (int i = 0; i < notes.Count; i++)
                    {
                        Note n = foundNotes[i];
                        Console.Write("{0}. {1} {2}{5}. Phone number: +{3}. {4}. ", n.id, n.lastName, n.firstName, n.phoneNumber, n.country, n.middleName != null ? " " + n.middleName + " " : "");
                        if (n.dateOfBirth != new DateTime())
                            Console.Write("Date of Birth: {0}. ", n.dateOfBirth);
                        if (n.organisation != null && n.organisation != "")
                            Console.Write("Organisation: {0}. ", n.organisation);
                        if (n.position != null && n.position != "")
                            Console.Write("Position: {0}. ", n.position);
                        if (n.other != null && n.other != "")
                            Console.Write("\nOther: {0}", n.other);
                        Console.WriteLine();
                    }
                }
                catch (Exception)   // Somethimes can see a problem in here because of the 'Out of range' exception.
                {
                }
            }
        }

        private static void ChangeId(ref List<Note> notes, int id)
        {
            for (int i = --id; i < notes.Count; i++)
                notes[i].id--;
        }
    }
}
